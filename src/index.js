const path = require("path");
const http = require("http");
const express = require("express");
const socketio = require("socket.io");

const app = express();
const server = http.createServer(app);
const io = socketio(server);

const { join, disconnect } = require("./socket/connectionsToRoom");
const { locationMessage, message } = require("./socket/messages");

const port = process.env.PORT || 3000;
const publicDirectoryPath = path.join(__dirname, "../public");

app.use(express.static(publicDirectoryPath));

io.on("connection", socket => {
  /// join
  join(socket, io);
  ///messages
  message(socket, io);
  ///location
  locationMessage(socket, io);
  ///disconnect
  disconnect(socket, io);
});

server.listen(port, () => {
  console.log(`App up and running on port ${port}`);
});
