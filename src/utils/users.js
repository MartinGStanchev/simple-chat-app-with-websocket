const users = [];

// addUser
const addUser = ({ id, username, room }) => {
  // Clean the data
  username = username.trim().toLowerCase();
  room = room.trim().toLowerCase();
  // Validate the data
  if (!username || !room) {
    return {
      error: "Username and room are required!"
    };
  }

  // Check for exsisting user
  const exsistingUser = users.find(user => {
    return user.room === room && user.username === username;
  });
  if (exsistingUser) {
    return {
      error: "This username is already taken!"
    };
  }
  // Add them in the room
  const user = { id, username, room };
  users.push(user);
  return { user };
};

// removeUser
const removeUser = id => {
  const index = users.findIndex(user => user.id === id);

  if (index !== -1) {
    return users.splice(index, 1)[0];
  }
};

// fetchUser
const fetchUser = id => users.find(user => user.id == id);
// getUsersInRoom
const getUsersInRoom = room => users.filter(user => user.room === room);

module.exports = {
    addUser,
    removeUser,
    fetchUser,
    getUsersInRoom
}