const { fetchUser } = require("../utils/users");
const Filter = require("bad-words");
const { io } = require("../index");
const {
  generateMessage,
  generateLocationMessage
} = require("../utils/messages");

const message = (socket, io) => {
  socket.on("sendMessage", (message, callback) => {
    const filter = new Filter();
    const user = fetchUser(socket.id);

    if (filter.isProfane(message)) {
      return callback("Profanity is not allowed!");
    }
    io.to(user.room).emit("message", generateMessage(message, user.username)); ///emit *
    callback();
  });
};

const locationMessage = (socket, io) => {
  socket.on("sendLocation", (coords, callback) => {
    const user = fetchUser(socket.id);
    io.to(user.room).emit(
      "locationMessage",
      generateLocationMessage(
        `https://www.google.com/maps?q=${coords.latitude},${coords.longitude}`,
        user.username
      )
    );
    callback("Location shared!");
  });
};

module.exports = {
  message,
  locationMessage
};
