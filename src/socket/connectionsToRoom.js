const { addUser, removeUser, getUsersInRoom } = require("../utils/users");
const { generateMessage } = require("../utils/messages");
const { capitalize } = require("../helpers/capitalize");

const join = (socket, io) => {
  socket.on("join", ({ username, room }, callback) => {
    const { error, user } = addUser({ id: socket.id, username, room });

    if (error) {
      return callback(error);
    }
    socket.join(user.room);

    socket.emit("message", generateMessage("Welcome!", "Admin"));
    socket.broadcast
      .to(user.room)
      .emit(
        "message",
        generateMessage(
          `${capitalize(user.username)} has joined!`,
          user.username
        )
      ); ///emit * but me
    io.to(user.room).emit("roomData", {
      room: user.room,
      users: getUsersInRoom(user.room)
    });
    callback();
  });
};

const disconnect = (socket, io) => {
  socket.on("disconnect", () => {
    const user = removeUser(socket.id);
    if (user) {
      io.to(user.room).emit(
        "message",
        generateMessage(`${capitalize(user.username)} has left!`, user.username)
      );
      io.to(user.room).emit("roomData", {
        room: user.room,
        users: getUsersInRoom(user.room)
      });
    }
  });
};

module.exports = {
  join,
  disconnect
};
